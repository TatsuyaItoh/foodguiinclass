import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodGuiInClass {
    private JPanel root;
    private JButton tempraButton;
    private JButton ramenButton;
    private JButton udonButton;
    private JTextArea textArea1;

    void conf(String FoodName){
        int confiramation =JOptionPane.showConfirmDialog(null,
                "Would you like to order "+FoodName,
                "order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if (confiramation == 0){
            JOptionPane.showMessageDialog(null, "Order for "+FoodName+" received.");
            //textArea1.setText("Order for "+FoodName+" received.");
        }
    }

    public FoodGuiInClass() {
        tempraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                conf("tempra");
            }
        });


        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                conf("ramen");
            }
        });


        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                conf("udon");
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoofGUI");
        frame.setContentPane(new FoodGuiInClass().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
